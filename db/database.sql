-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- G�n�r� le :  Mer 25 Mai 2016 � 09:24
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de donn�es :  `trail_de_la_mine`
--

-- --------------------------------------------------------

--
-- Structure de la table `table_categorie`
--

DROP TABLE IF EXISTS `table_categorie`;
CREATE TABLE IF NOT EXISTS `table_categorie` (
  `id_categorie` int(200) NOT NULL AUTO_INCREMENT,
  `type_categorie` varchar(50) NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `table_coureur`
--

DROP TABLE IF EXISTS `table_coureur`;
CREATE TABLE IF NOT EXISTS `table_coureur` (
  `id_coureur` int(200) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `date_naiss` date NOT NULL,
  `sexe` varchar(6) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `code_postal` int(5) NOT NULL,
  `ville` varchar(20) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `E-mail` varchar(50) NOT NULL,
  `Club` int(50) NOT NULL,
  PRIMARY KEY (`id_coureur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `table_edition`
--

DROP TABLE IF EXISTS `table_edition`;
CREATE TABLE IF NOT EXISTS `table_edition` (
  `id_edition` int(200) NOT NULL AUTO_INCREMENT,
  `nom_edition` varchar(50) NOT NULL,
  PRIMARY KEY (`id_edition`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `table_epreuve`
--

DROP TABLE IF EXISTS `table_epreuve`;
CREATE TABLE IF NOT EXISTS `table_epreuve` (
  `id_epreuve` int(200) NOT NULL AUTO_INCREMENT,
  `description_epreuve` varchar(200) NOT NULL,
  PRIMARY KEY (`id_epreuve`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `table_inscription`
--

DROP TABLE IF EXISTS `table_inscription`;
CREATE TABLE IF NOT EXISTS `table_inscription` (
  `num_inscription` int(50) NOT NULL AUTO_INCREMENT,
  `doc_inscription` int(200) NOT NULL,
  `validation_inscription` tinyint(1) NOT NULL,
  `validation_regelement` tinyint(1) NOT NULL,
  `date_inscription` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dossard` int(200) NOT NULL,
  `temps` time NOT NULL,
  PRIMARY KEY (`num_inscription`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `table_reglement`
--

DROP TABLE IF EXISTS `table_reglement`;
CREATE TABLE IF NOT EXISTS `table_reglement` (
  `id_reglement` int(200) NOT NULL AUTO_INCREMENT,
  `num_cheque` varchar(50) NOT NULL,
  `banque` varchar(50) NOT NULL,
  `date_reglement` date NOT NULL,
  PRIMARY KEY (`id_reglement`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
