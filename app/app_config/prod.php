<?php

// Doctrine (db)
$app['db.options'] = array(
    'charset'  => 'utf8',
    'host'     => 'localhost',
    'port'     => '8080',
    'dbname'   => 'Trail_de_la_mine',
    'user'     => 'root',
    'password' => '',
);