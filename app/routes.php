<?php

// Home page
$app->get('/', function () use ($app) {
    $coureurs = $app['db_creator']->findAll();
//    ob_start();             // start buffering HTML output
//    require '../views/view.php';
//    $view = ob_get_clean(); // assign HTML output to $view
//    return $view;
    
    return $app['twig']->render('index.html.twig', array('coureurs' => $coureurs));
});