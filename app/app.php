<?php

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
//use src\DAO\CoureurDAO;


ErrorHandler::register();
ExceptionHandler::register();

$app['db.options'] = array(
    'driver'   => 'pdo_mysql',
    'charset'  => 'utf8',
    'host'     => 'localhost',
//    'port'     => '8080',
    'dbname'   => 'trail_de_la_mine',
    'user'     => 'root',
    'password' => '',
);

$app->register(new Silex\Provider\DoctrineServiceProvider());

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views'
    ));

//$app['dao.coureur'] = $app->share(function ($app) {
//    return new Trail_de_la_mine\src\DAO\CoureurDAO ($app['db']);
//});

$app['db_creator'] = $app->share(function($app) {
       return new src\DAO\CoureurDAO($app['db']);
    });
    
$app['db_creator']->findAll();    
    

//
//$app['db'] = $app->share($app->extend('db', function(\Doctrine\DBAL\Connection $db)use($app) {
//    return $db;
//}));