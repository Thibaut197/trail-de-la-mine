<?php

namespace src\entity;

use Doctrine\DBAL\Connection;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorie
 *
 * @author thibault
 */

Class Categorie {
    
    private $id_categorie;
    
    private $type_categorie;
    
    function getId_categorie() {
        return $this->id_categorie;
    }

    function getType_categorie() {
        return $this->type_categorie;
    }

    function setId_categorie($id_categorie) {
        $this->id_categorie = $id_categorie;
    }

    function setType_categorie($type_categorie) {
        $this->type_categorie = $type_categorie;
    }

}

