<?php

namespace src\entity;

use Doctrine\DBAL\Connection;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorie
 *
 * @author thibault
 */

Class Epreuve {
    
    private $id_epreuve;
    
    private $description_epreuve;
    
    function getId_epreuve() {
        return $this->id_epreuve;
    }

    function getDescription_epreuve() {
        return $this->description_epreuve;
    }

    function setId_epreuve($id_epreuve) {
        $this->id_epreuve = $id_epreuve;
    }

    function setDescription_epreuve($description_epreuve) {
        $this->description_epreuve = $description_epreuve;
    }
    
}
    