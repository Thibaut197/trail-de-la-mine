<?php

namespace src\entity;

use Doctrine\DBAL\Connection;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorie
 *
 * @author thibault
 */

Class Reglement {
    
    private $id_reglement;
    
    private $num_cheque;
    
    private $banque;
    
    private $dta_reglement;
    
    function getId_reglement() {
        return $this->id_reglement;
    }

    function getNum_cheque() {
        return $this->num_cheque;
    }

    function getBanque() {
        return $this->banque;
    }

    function getDta_reglement() {
        return $this->dta_reglement;
    }

    function setId_reglement($id_reglement) {
        $this->id_reglement = $id_reglement;
    }

    function setNum_cheque($num_cheque) {
        $this->num_cheque = $num_cheque;
    }

    function setBanque($banque) {
        $this->banque = $banque;
    }

    function setDta_reglement($dta_reglement) {
        $this->dta_reglement = $dta_reglement;
    }
    
}
    