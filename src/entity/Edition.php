<?php

namespace src\entity;

use Doctrine\DBAL\Connection;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorie
 *
 * @author thibault
 */

Class Edition {
    
    private $id_edition;
    
    private $nom_edition;
    
    function getId_edition() {
        return $this->id_edition;
    }

    function getNom_edition() {
        return $this->nom_edition;
    }

    function setId_edition($id_edition) {
        $this->id_edition = $id_edition;
    }

    function setNom_edition($nom_edition) {
        $this->nom_edition = $nom_edition;
    }
    
}
    