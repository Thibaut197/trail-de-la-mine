<?php

namespace src\entity;

use Doctrine\DBAL\Connection;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorie
 *
 * @author thibault
 */

Class Inscription {
    
    private $num_inscription;
    
    private $doc_inscription;
    
    private $validation_inscription;
    
    private $validation_reglement;
    
    private $date_inscription;
    
    private $dossard;
    
    private $temps;
    
    function getNum_inscription() {
        return $this->num_inscription;
    }

    function getDoc_inscription() {
        return $this->doc_inscription;
    }

    function getValidation_inscription() {
        return $this->validation_inscription;
    }

    function getValidation_reglement() {
        return $this->validation_reglement;
    }

    function getDate_inscription() {
        return $this->date_inscription;
    }

    function getDossard() {
        return $this->dossard;
    }

    function getTemps() {
        return $this->temps;
    }

    function setNum_inscription($num_inscription) {
        $this->num_inscription = $num_inscription;
    }

    function setDoc_inscription($doc_inscription) {
        $this->doc_inscription = $doc_inscription;
    }

    function setValidation_inscription($validation_inscription) {
        $this->validation_inscription = $validation_inscription;
    }

    function setValidation_reglement($validation_reglement) {
        $this->validation_reglement = $validation_reglement;
    }

    function setDate_inscription($date_inscription) {
        $this->date_inscription = $date_inscription;
    }

    function setDossard($dossard) {
        $this->dossard = $dossard;
    }

    function setTemps($temps) {
        $this->temps = $temps;
    }
    
}
    