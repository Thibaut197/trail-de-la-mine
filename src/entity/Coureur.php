<?php

namespace src\entity;

use Doctrine\DBAL\Connection;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Coureur
 *
 * @author thibault
 */
class Coureur {
    
    
    
    private $id;
   
    private $nom;
    
    private $prenom;
    
    private $date_naiss;
    
    private $sexe;
    
    private $adresse;
            
    private $code_postal;
            
    private $ville;
    
    private $telephone;
            
    private $email;
            
    private $club;
    
    //    function __construct($id, $nom, $prenom, $date_naiss, $sexe, $adresse, $code_postale, $ville, $telephone, $email, $club) {
//        $this->id = $id;
//        $this->nom = $nom;
//        $this->prenom = $prenom;
//        $this->date_naiss = $date_naiss;
//        $this->sexe = $sexe;
//        $this->adresse = $adresse;
//        $this->code_postal = $code_postal;
//        $this->ville = $ville;
//        $this->telephone = $telephone;
//        $this->email = $email;
//        $this->club = $club;
//    }

    function getId() {
        return $this->id;
    }
   
    function getNom() {
        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    function getDate_naiss() {
        return $this->date_naiss;
    }

    function getSexe() {
        return $this->sexe;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getCode_postal() {
        return $this->code_postal;
    }

    function getVille() {
        return $this->ville;
    }

    function getTelephone() {
        return $this->telephone;
    }

    function getemail() {
        return $this->email;
    }

    function getclub() {
        return $this->club;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setNom($nom) {
        $this->nom = $nom;
    }

    function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    function setDate_naiss($date_naiss) {
        $this->date_naiss = $date_naiss;
    }

    function setSexe($sexe) {
        $this->sexe = $sexe;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setCode_postal($code_postal) {
        $this->code_postal = $code_postal;
    }

    function setVille($ville) {
        $this->ville = $ville;
    }

    function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    function setemail($email) {
        $this->email = $email;
    }

    function setclub($club) {
        $this->club = $club;
    }
}   