<?php

namespace Trail_de_la_mine\src;

class coureur 
{
  
    private $id;

    private $nom;

    private $prenom;
    
    private $date_naiss;
    
    private $sexe;
    
    private $adresse;
    
    private $code_postal;
    
    private $ville;
    
    private $telephone;
    
    private $email;
    
    private $club;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getnom() {
        return $this->nom;
    }

    public function setnom($nom) {
        $this->nom = $nom;
    }

    public function getprenom() {
        return $this->prenom;
    }

    public function setprenom($nom) {
        $this->prenom = $prenom;
    }
    
    public function getdate_naiss() {
        return $this->date_naiss;
    }

    public function setdate_naiss($date_naiss) {
        $this->nom = $date_naiss;
    }
    public function getsexe() {
        return $this->sexe;
    }

    public function setsexe($sexe) {
        $this->sexe = $sexe;
    }
    public function getadresse() {
        return $this->adresse;
    }

    public function setadresse($adresse) {
        $this->adresse = $adresse;
    }
    public function getcode_postal() {
        return $this->code_postal;
    }

    public function setcode_postal($code_postal) {
        $this->code_postal = $code_postal;
    }
    public function getville() {
        return $this->ville;
    }

    public function setville($ville) {
        $this->ville = $ville;
    }
    public function gettelephone() {
        return $this->telephone;
    }

    public function setelephone($telephone) {
        $this->telephone = $telephone;
    }
    public function getemail() {
        return $this->email;
    }

    public function setemail($email) {
        $this->email = $email;
    }
    public function getclub() {
        return $this->club;
    }

    public function setclub($club) {
        $this->club = $club;
    }
}