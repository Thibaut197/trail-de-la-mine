<?php

namespace src\DAO;

use Doctrine\DBAL\Connection;
use src\entity\Coureur;

class CoureurDAO
{
    
    private $db;
    
    
    public function __construct(Connection $db) {
        $this->db = $db;
      }
      
    public function findAll() {
        $sql = "select * from Coureur order by id_coureur desc";
        $result = $this->db->fetchAll($sql);
        
        $coureurs = array();
        foreach ($result as $row) {
            $id_coureur = $row['id_coureur'];
            $coureurs[$id_coureur] = $this->buildCoureur($row);
    }
        return $coureurs;
    }    
        
    private function buildCoureur(array $row) {
        $coureurs = new Coureur();
        $coureurs->setId($row['id_coureur']);
        $coureurs->setNom($row['nom']);
        $coureurs->setPrenom($row['prenom']);
        $coureurs->setDate_naiss($row['date_naiss']);
        $coureurs->setSexe($row['sexe']);
        $coureurs->setadresse ($row['adresse']);
        $coureurs->setCode_postal($row['code_postal']);
        $coureurs->setVille($row['ville']);
        $coureurs->setTelephone($row['telephone']);
        $coureurs->setemail($row['email']);
        $coureurs->setClub($row['club']);
        
        return $coureurs;
    }
    
    }
    
      
